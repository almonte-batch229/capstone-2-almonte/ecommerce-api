const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers")
const auth = require("../auth")

// User Registration
router.post("/register", (req, res) => {
	userControllers.registerUser(req.body).then(resultFromController => {
		res.send(resultFromController);
	});
});

// User Authentication
router.post("/login", (req,res) => {

	userControllers.loginUser(req.body).then(resultFromController => {
		res.send(resultFromController);
	});
});

// User Checkout
router.post("/checkout", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	const data = {
		userId: userData.id, 
		reqBody: req.body
	};

	if(userData.isAdmin == false) {
		userControllers.userCheckout(data).then(resultFromController => {
			res.send(resultFromController);
		});
	}
	else {
		res.send("You are not authorized to create orders.")
	}
});

// Retrieve User Details
router.get("/details", auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization);

	userControllers.getUserDetails(userData.id).then(resultFromController => {
		res.send(resultFromController);
	});
});

// Set User as Admin
router.put("/admin/:userId", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin === true) {
		userControllers.setUserAdmin(req.params.userId).then(resultFromController => {
			res.send(resultFromController);
		});
	}
	else {
		res.send("You are not authorized to commit changes.")
	}
});

// Retrieve Authenticated User's Order
router.get("/orders/:userId", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin == false) {
		userControllers.getUserOrders(req.params.userId).then(resultFromController => {
			res.send(resultFromController);
		});
	}
	else {
		res.send("You are not authorized to retrieve user orders.")
	}
});

// Retrieve All Orders
router.get("/allorders", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin == true) {
		userControllers.getAllOrders().then(resultFromController => {
			res.send(resultFromController);
		});
	}
	else {
		res.send("You are not authorized to retrieve all orders.")
	}
});

module.exports = router;