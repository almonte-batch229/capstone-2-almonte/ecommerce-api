const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers");
const auth = require("../auth");

// Create a Product
router.post("/create", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin === true) {
		productControllers.createProduct(req.body).then(resultFromController => {
			res.send(resultFromController);
		});
	}
	else {
		res.send("You are not authorized to commit changes.");
	}
});

// Retrieve All Products
router.get("/all", (req, res) => {

	productControllers.getAllProducts().then(resultFromController => {
		res.send(resultFromController);
	});
});

// Retrieve All Active Products
router.get("/active", (req, res) => {

	productControllers.getAllActiveProducts().then(resultFromController => {
		res.send(resultFromController);
	});
});

// Retrieve Single Product
router.get("/:productId", (req, res) => {
	
	productControllers.getProduct(req.params).then(resultFromController => {
		res.send(resultFromController);
	});
});

// Update Product Information
router.put("/update/:productId", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin === true) {
		productControllers.updateProductInfo(req.params, req.body).then(resultFromController => {
			res.send(resultFromController);
		});
	}
	else {
		res.send("You are not authorized to commit changes.")
	}
});

// Archive a Product
router.put("/archive/:productId", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin === true) {
		productControllers.archiveProduct(req.params).then(resultFromController => {
			res.send(resultFromController);
		});
	}
	else {
		res.send("You are not authorized to commit changes.")
	}
});

// Retrieve a Product
router.put("/retrieve/:productId", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin === true) {
		productControllers.retrieveProduct(req.params).then(resultFromController => {
			res.send(resultFromController);
		});
	}
	else {
		res.send("You are not authorized to commit changes.")
	}
});

module.exports = router;