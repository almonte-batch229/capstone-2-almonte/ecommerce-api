const User = require("../models/User");
const Order = require("../models/Order");
const Product = require("../models/Product")
const bcrypt = require("bcrypt");
const auth = require("../auth");

// User Registration
module.exports.registerUser = (reqBody) => {

	return User.find({email: reqBody.email}).then(user => {

		if(user.length > 0) {

			return false;

		} else {

			return User.find({username: reqBody.username}).then(uname_result => {
			
				if(uname_result > 0) {

					return false;

				} else {

					let newUser = new User({
						email: reqBody.email,
						password: bcrypt.hashSync(reqBody.password, 10),
						username: reqBody.username,
						contactNo: reqBody.contactNo,
					});

					return newUser.save().then((newuser, error) => {

						if(error) {

							return false;

						} else {
							
							return true;
						}
					});
				}
			});
		}
	});
};

// User Authentication
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(user => {

		if (user !== null) {

			let isPasswordCorrect = bcrypt.compareSync(reqBody.password, user.password);

			if(isPasswordCorrect) {
				return { access_token: auth.createAccessToken(user)};
			} 
			else {
				return false;
			}

		} 
		else {

			return false;
		}
	});
};

// User Checkout
module.exports.userCheckout = (data) => {

	return Product.findById(data.reqBody.productId).then(product => {

		if(product == null) {
			return "The product you are trying to purchase is not available."
		}
		else {

			let newOrder = new Order({
				userId: data.userId,
				products: {
					productName: product.name,
					quantity: data.reqBody.quantity
				},
				totalAmount: product.price * data.reqBody.quantity
			});

			return newOrder.save().then((order, error) => {

				if(error) {
					console.log(error);
					return false;
				}
				else {
					console.log(order)
					return true;
				}
			});
		};
	});
};

// Retrieve User Details
module.exports.getUserDetails = (userId) => {

	return User.findById(userId).then(user => {

		user.password = "";

		return user;
	});
};

// Set User as Admin
module.exports.setUserAdmin = (reqParams) => {

	let setAsAdmin = {
		isAdmin: true
	};

	return User.findByIdAndUpdate(reqParams, setAsAdmin).then((user, error) => {

		if(error) {
			console.log(error);
			return false;
		}
		else {
			console.log(user)
			return true;
		}
	});
};

// Retrieve Authenticated User's Order
module.exports.getUserOrders = (reqParams) => {

	return Order.find({userId: reqParams}).then(order => {
		return order;
	});
};	

// Retrieve All Orders
module.exports.getAllOrders = () => {

	return Order.find().then(order => {
		return order;
	});
}