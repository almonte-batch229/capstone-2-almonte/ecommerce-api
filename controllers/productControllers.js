const Product = require("../models/Product")

// Create a Product
module.exports.createProduct = (reqBody) => {

	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});

	return newProduct.save().then((product, error) => {

		if(error) {
			console.log(error);
			return false;
		} 
		else {
			console.log(product);
			return true;
		}
	});
};

// Retrieves All Products
module.exports.getAllProducts = () => {

	return Product.find({}).then(product => {
		return product;
	});
};

// Retrieves All Active Products
module.exports.getAllActiveProducts = () => {

	return Product.find({isActive: true}).then(product => {
		return product;
	});
};

// Retrieve Single Product
module.exports.getProduct = (reqParams) => {

	return Product.findById(reqParams.productId).then(product => {
		return product;
	});
};

// Update Product Information
module.exports.updateProductInfo = (reqParams, reqBody) => {

	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
		
		if(error) {
			console.log(error);
			return false;
		} 
		else {
			console.log(product);
			return true;
		}
	});
};

// Archive a Product
module.exports.archiveProduct = (reqParams) => {

	let archivedCourse = {
		isActive: false
	};

	return Product.findByIdAndUpdate(reqParams.productId, archivedCourse).then((product, error) => {

		if(error) {
			console.log(error);
			return false;
		} 
		else {
			console.log(product);
			return true;
		}
	});
};

// Archive a Product
module.exports.retrieveProduct = (reqParams) => {

	let archivedCourse = {
		isActive: true
	};

	return Product.findByIdAndUpdate(reqParams.productId, archivedCourse).then((product, error) => {

		if(error) {
			console.log(error);
			return false;
		} 
		else {
			console.log(product);
			return true;
		}
	});
};
